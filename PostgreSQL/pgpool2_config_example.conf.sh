#------------------------------Install Rhel-------------------------------#
# dnf install -y https://www.pgpool.net/yum/rpms/4.5/redhat/rhel-8-x86_64/pgpool-II-release-4.5-1.noarch.rpm
# dnf install -y pgpool-II-pg16-*

# Criando diretório e dando as devidas permissões para salvas os logs:
mkdir -p /var/log/pgpool2/
chown -R postgres:postgres /var/log/pgpool2

# Exemplo 01 de configuração do PgPool2 para autenticação com o PostgreSQL em MD5.
# Abaixo o segundo exemplo sem autenticação MD5
## vim /etc/pgpool-II/pgpool.conf
### Conteúdo abaixo:

#---------------------------Listen addresses------------------------------#
listen_addresses = '0.0.0.0'
port = 9999
pcp_port = 9898

#---------------Load balancing method (usando replicação)-----------------#
load_balance_mode = on
backend_clustering_mode = 'streaming_replication'

#--------------------------Node Primary-----------------------------------#
backend_hostname0 = '110.100.5.240'
backend_port0 = 5432
# backend_weight0 = 0 (Escritas são realizadas somente no primary) 
# backend_weight0 = 1 ( As consultas são realizadas no PG primary e secondary)
backend_weight0 = 0
backend_data_directory0 = '/path/data/directory/postgres'
backend_flag0 = 'DISALLOW_TO_FAILOVER'

#---------------------------Node Secondary--------------------------------#
backend_hostname1 = '110.100.5.241'
backend_port1 = 5432
backend_weight1 = 1
# backend_weight0 = 0 (Escritas são realizadas somente no primary) 
# backend_weight0 = 1 ( As consultas são realizadas no PG primary e secondary)
backend_data_directory1 = '/path/data/directory/postgres'
backend_flag1 = 'DISALLOW_TO_FAILOVER'

#-----------------------------Connections---------------------------------#
authentication_timeout = 60
num_init_children = 300
num_backend_nodes = 2
max_pool = 1				    # max_pool x num_init_children = total connections
child_life_time = 300  			#Terminate idle connections in seconds.
#
pool_passwd = 'pool_passwd'
enable_pool_hba = on

#------------------------------Logs---------------------------------------#
log_destination = 'stderr'
logging_collector = on
log_directory = '/var/log/pgpool2'
log_filename = 'pgpool2%Y-%m-%d_%H.log'

#-------------------------------Manager-----------------------------------#
sr_check_user = 'admin_user_pg'
sr_check_password = '************'
#
health_check_user = 'admin_user_pg'
health_check_password = '************'
#
health_check_max_retries = 0
health_check_retry_delay = 10
health_check_timeout = 10
auto_failback = off 
#-------------------------------------------------------------------------#


# Exemplo 02 de configuração do PgPool2 para direcionar a autenticação ao PostgreSQL com pg_hba nativo:
## vim /etc/pgpool-II/pgpool.conf
### Conteúdo abaixo:

#---------------------------Listen addresses------------------------------#
listen_addresses = '0.0.0.0'
port = 9999
pcp_port = 9898

#---------------Load balancing method (usando replicação)-----------------#
load_balance_mode = on
backend_clustering_mode = 'streaming_replication'

#--------------------------Node Primary-----------------------------------#
backend_hostname0 = '110.100.5.240'
backend_port0 = 5432
backend_weight0 = 1
backend_data_directory0 = '/path/data/directory/postgres'
backend_flag0 = 'DISALLOW_TO_FAILOVER'

#---------------------------Node Secondary--------------------------------#
backend_hostname1 = '110.100.5.241'
backend_port1 = 5432
backend_weight1 = 1
backend_data_directory1 = '/path/data/directory/postgres'
backend_flag1 = 'DISALLOW_TO_FAILOVER'

#-----------------------------Connections---------------------------------#
authentication_timeout = 60
num_init_children = 300
num_backend_nodes = 2
max_pool = 1				    # max_pool x num_init_children = total connections
child_life_time = 300  			#Terminate idle connections in seconds.
#
enable_pool_hba = off
allow_clear_text_frontend_auth = on
pool_passwd = ''

#------------------------------Logs---------------------------------------#
log_destination = 'stderr'
logging_collector = on
log_directory = '/var/log/pgpool2'
log_filename = 'pgpool2%Y-%m-%d_%H.log'

#-------------------------------Manager-----------------------------------#
sr_check_user = 'admin_user_pg'
sr_check_password = '************'
#
health_check_user = 'admin_user_pg'
health_check_password = '************'
#
health_check_max_retries = 0
health_check_retry_delay = 10
health_check_timeout = 10
auto_failback = off 
#-------------------------------------------------------------------------#

# Caso esteja utilizando o exemplo 02, pule estes passos das autenticações!!!
# Acesso o PG e colete o user e senha em MD5 do usuário que precisa se autenticar via pgpool:
SELECT rolname || ':' || md5(rolpassword) AS usuario_senha
FROM pg_authid
WHERE rolname = 'nexfar';

# Caso estiver como SCRAM-SHA:
psql -Atq -U postgres -d postgres -c "SELECT concat(usename, ':', passwd) FROM pg_shadow;"

# Com o resultado em mãos, salve no arquivo pool_passwd (/etc/pgpool-II/pool_passwd)
caio:03bbe9433c32cda7a33bd85d7c7f4413  (md5)
caio:SCRAM-SHA-256$4096:tKfasE+CG5elT7aORA1I0Q==$VBaeH8+476hq23PjV7hplWhhRLhK7FHR2Xgbqx0T2YQ=:LXnElyXJ8gVuppZ7FzEX3jgXvXSiv3Dy16pw2cOCRts=   (SCRAM-SHA)

systemctl start pgpool
systemctl enable pgpool
systemctl reload pgpool

# Pronto, agora basta apontar sua aplicação para o PgPool.
# Infomando:
Endereço ip do pgpool2;
Seu usuário;
Sua senha;
Porta 9999;
Database.
