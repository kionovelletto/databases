#!/bin/bash

######################################################################
#                                                                    #
#	Este script realiza o tuning no PostgreSQL.                      #
#   ⚠️Script em testes⚠️                                             #
#                                                                    #
#                                                                    #
######################################################################


# Este script realiza o tuning no PostgreSQL.

# Tamanho do cache de memória: O cache de memória é usado para armazenar dados frequentemente acessados. Aumentar o tamanho do cache de memória pode melhorar o desempenho de consultas e operações.
# Tamanho da tabela de hash: A tabela de hash é usada para armazenar índices. Aumentar o tamanho da tabela de hash pode melhorar o desempenho de consultas que usam índices.
# Tamanho do pool de conexão: O pool de conexão é usado para armazenar conexões abertas ao PostgreSQL. Aumentar o tamanho do pool de conexão pode melhorar o desempenho de aplicativos que usam muitas conexões ao PostgreSQL.
# Tamanho do buffer de spool: O buffer de spool é usado para armazenar dados de log. Aumentar o tamanho do buffer de spool pode melhorar o desempenho de aplicativos que geram muitos dados de log.
# Tamanho do buffer de commit: O buffer de commit é usado para armazenar dados que estão sendo gravados no disco. Aumentar o tamanho do buffer de commit pode melhorar o desempenho de aplicativos que realizam muitas operações de gravação.
# Número de worker threads: Os worker threads são usados para executar consultas e operações. Aumentar o número de worker threads pode melhorar o desempenho de aplicativos que executam muitas consultas e operações.
# Tamanho do buffer de writeback: O buffer de writeback é usado para armazenar dados que ainda não foram gravados no disco. Aumentar o tamanho do buffer de writeback pode melhorar o desempenho de aplicativos que geram muitos dados de log.

if ! command -v psql >/dev/null; then
  echo "O PostgreSQL não está instalado. ❌"
  exit 1
fi

if [ ! -f /etc/postgresql/{<version>}/main/postgresql.conf ]; then
  echo "O arquivo de configuração do PostgreSQL não existe. ❌"
  exit 1
fi

# READY OS
mem_total=$(free -m | awk '{print $2}')
num_cpu=$(nproc)

# CACHE MEM
echo "Ajustando o tamanho do cache de memória... ✔️"
psql -c "ALTER SYSTEM SET shared_buffers = $mem_total / 4;"

# TABLE HASH
echo "Ajustando o tamanho da tabela de hash... ✔️"
psql -c "ALTER SYSTEM SET hash_max_size = $mem_total / 2;"

# POOL CONNECTION
echo "Ajustando o tamanho do pool de conexão... ✔️"
psql -c "ALTER SYSTEM SET max_connections = $num_cpu * 2;"

# BUFFER SPOOL
echo "Ajustando o tamanho do buffer de spool... ✔️"
psql -c "ALTER SYSTEM SET wal_buffers = $mem_total / 16;"

# BUFFER COMMIT
echo "Ajustando o tamanho do buffer de commit... ✔️"
psql -c "ALTER SYSTEM SET commit_delay = 0;"

# WORKER THREADS
echo "Ajustando o número de worker threads... ✔️"
psql -c "ALTER SYSTEM SET max_worker_processes = $num_cpu * 2;"

# BUFFER WRITEBACK
echo "Ajustando o tamanho do buffer de writeback... ✔️"
psql -c "ALTER SYSTEM SET wal_writer_delay = 0;"

# BUFFER WAL
echo "Ajustando o tamanho do buffer de WAL... ✔️"
psql -c "ALTER SYSTEM SET wal_buffers = $mem_total / 16;"

# WRITEBACK BUFFER
echo "Ajustando o tamanho do buffer de writeback... ✔️"
psql -c "ALTER SYSTEM SET wal_writer_delay = 0;"

# RESTART POSTGRESQL
echo "Reiniciando o PostgreSQL... ✔️"
systemctl restart postgresql

# DONE
echo "O tuning foi realizado com sucesso. ✅"
