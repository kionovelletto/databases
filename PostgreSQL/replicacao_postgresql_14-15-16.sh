#------------------------Instalando o Postgresql 16 em distribuição Rhel----------------------------#
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf -qy module disable postgresql
sudo dnf install -y postgresql16-server
sudo /usr/pgsql-16/bin/postgresql-16-setup initdb
sudo -u postgres /usr/pgsql-16/bin/initdb -D /data/pgdata
sudo -u postgres /usr/pgsql-16/bin/pg_ctl -D /data/pgdata -l logfile start
sudo systemctl daemon-reload
sudo systemctl enable postgresql-16
sudo systemctl start postgresql-16

# Configure o pg:
vim ../postgresql.conf
data_directory = '/data/pgdata'
hba_file = '/data/pgdata/pg_hba.conf'
autovacuum = on
autovacuum_vacuum_threshold = 50
autovacuum_analyze_threshold = 50
autovacuum_vacuum_scale_factor = 0.2
autovacuum_analyze_scale_factor = 0.1

#--------------------------------Configuração a replicação do PG-----------------------------------------#
# Válido para as versões 14/15/16

# Crie o arquivo abaixo no diretório do postgreSQL:
touch ../standby.signal
chown -R postgres:postgres ../standby.signal

# Criar usuário para replicação do pg:
CREATE USER replication REPLICATION LOGIN ENCRYPTED PASSWORD '********';

# Add no postgresql.conf do pg primário:
wal_level = replica
max_wal_senders = 10
primary_conninfo = 'host=primary_ip_address port=5432 user=replication password=*******'

# Add no pg_hba.conf do pg primário:
host    replication     replication     IPaddressReplica   md5

# Gere e troque as chaves entre os pgs:
ssh-keygen
cat ~/.ssh/id_rsa.pub
vim ~/.ssh/authorized_keys

# No pg slave, gere a cópia do cluster pg primario para o slave:
pg_basebackup -D /data/pgdata -h 192.169.100.90 -X stream -c fast -U replication -W -v -P
(Inserir a senha do user replication e endereço)

# No diretório do pg slave, crie os arquivos abaixo e adicione as configurações:
vim postgresql.conf
standby_mode = on

vim standby.signal
standby_mode = on 
primary_conninfo = 'host=primary_ip_address port=5432 user=replication password=*******'

sudo systemctl start postgresql-16
sudo systemctl enable postgresql-16

# Promover a réplica para master:
pg_ctl promote ou pg_promote();


# Executando dump no pg réplica:
hot_standby_feedback = true
hot_standby_feedback = off
