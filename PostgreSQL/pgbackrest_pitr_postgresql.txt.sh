#-----------------Instalação em distro Rhel--------------------#
# https://pgbackrest.org/user-guide-rhel.html#build
## Instale o pgbackrest tanto no servidor postgres que será feito o backup, quando no servidor pgbackrest, ambos na mesma versão para o correto funcionamento

mkdir -p /build
wget -q -O - https://github.com/pgbackrest/pgbackrest/archive/release/2.49.tar.gz | tar zx -C /build

# (1) Instalando as Dependências(atente-se a versão do postgreSQL):
sudo dnf config-manager --set-enabled ol8_codeready_builder -y
sudo dnf install make gcc postgresql12-devel openssl-devel libpq-devel.x86_64 libxml2-devel lz4-devel libzstd-devel bzip2-devel libyaml-devel libssh2-devel vim -y
sudo dnf install postgresql-libs libssh2 -y

# (2) Inslação via dnf:
## Instale o pgbackrest tanto no servidor postgres que será feito o backup, quando no servidor pgbackrest, ambos na mesma versão para o correto funcionamento
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf -qy module disable postgresql
sudo dnf install -y postgresql16-server postgresql16-devel
sudo dnf install pgbackrest


# Acesse o diretório e faça a instalação compilada:
cd /build/pgbackrest-release-2.49/src && ./configure && make

# Execute:
sudo scp /build/pgbackrest-release-2.49/src/pgbackrest /usr/bin
sudo chmod 755 /usr/bin/pgbackrest

# Criação de arquivos e permissionamento:
sudo mkdir -p -m 770 /var/log/pgbackrest
sudo chown postgres:postgres /var/log/pgbackrest
sudo mkdir -p /etc/pgbackrest
sudo mkdir -p /etc/pgbackrest/conf.d
sudo touch /etc/pgbackrest/pgbackrest.conf
sudo chmod 640 /etc/pgbackrest/pgbackrest.conf
sudo chown postgres:postgres /etc/pgbackrest/pgbackrest.conf

# Criando o diretório para armazenar os backups:
mkdir -p /backup/pgbackrest
chown -R postgres:postgres /backup/pgbackrest

# Configurando o serviço do servidor pgbackrest:
vim /etc/pgbackrest/pgbackrest.conf

#--------------------------------------------------#
[global]
repo1-path=/backup/pgbackrest
repo1-retention-full=2
log-level-console=info
log-level-file=debug
process-max=16
start-fast=y
delta=y

[global:archive-push]
#compress-level=3

[srvpgprd01]
pg1-path=/var/lib/pgsql/12/data
pg1-host=EnderecoDoServidorPG
pg1-port=5432
pg1-user=postgres
#--------------------------------------------------#

# Configurando o pgbackrest no servidor do postgreSQL:
vim /etc/pgbackrest/pgbackrest.conf

#--------------------------------------------------#
[global]
repo1-host=IP_DO_SERVIDOR_PGBACKREST
repo1-path=/backup/pgbackrest
repo1-host-user=postgres
log-level-console=info
log-level-file=debug
process-max=16
start-fast=y
delta=y

[global:archive-push]
#compress-level=3

[srvpgprd01]
pg1-path=/var/lib/pgsql/12/data
pg1-port=5432
pg1-user=postgres
#--------------------------------------------------#

# Gere e troque as chaves entre os pgs:
ssh-keygen
cat ~/.ssh/id_rsa.pub
vim ~/.ssh/authorized_keys

# Configurar no postgresql.conf do servidor PG:
archive_command = 'pgbackrest --stanza=srvpgprd01 archive-push %p'
archive_mode = 'on'
log_filename = 'postgresql.log'
max_wal_senders = 3

systemctl restart postgresql-16

# No servidor do PgBackRest crie o stanza:
sudo -iu postgres pgbackrest --stanza=srvpgprd01 --log-level-console=info stanza-create

# Agora verifique se está tudo correto:
sudo -iu postgres pgbackrest --stanza=srvpgprd01 --log-level-console=info check

# Gerando backups:
Full: sudo -iu postgres pgbackrest --compress-level-network=0 --stanza=srvpgprd01 --type=full backup
Incremental: sudo -iu postgres pgbackrest --compress-level-network=0 --stanza=srvpgprd01 --type=incr backup
Diferencial: sudo -iu postgres pgbackrest --compress-level-network=0 --stanza=srvpgprd01 --type=diff backup

# Restaurando backups com PITR:
- Restauração para o último horário válido:
sudo -iu postgres pgbackrest --log-level-console=info --stanza=srvpgprd01  restore

- Delta(cluster pg completo): 
sudo -iu postgres pgbackrest --log-level-console=info --stanza=srvpgprd01 --compress-level-network=0 --archive-mode=off --delta --pg1-path=/data/restore \
	--repo1-path=/backup/pgbackrest --repo1-host=IP_DO_SERVIDOR_PGBACKREST --type=time "--target=2024-01-19 12:10:00" restore

- Single(Restaura apenas um database): 
sudo -iu postgres pgbackrest --log-level-console=info --stanza=srvpgprd01 --compress-level-network=0 --archive-mode=off --db-include=NOME_DATABASE \
    --type=time "--target=2024-01-19 12:00:00" --pg1-path=/data/restore --repo1-path=/backup/pgbackrest --repo1-host=IP_DO_SERVIDOR_PGBACKREST --force restore

## ATENÇÂO ##
Antes de iniciar o serviço do postgresql, troque a porta e comente a linha a linha archive_command(caso for teste de restauração, se for restauração para produção deixar.)
sudo -u postgres /usr/pgsql-16/bin/pg_ctl -D /data/restore -l logfile start

# Deletando stanza:
No servidor do postgresql e no pgbackrest, execute:
sudo -u postgres pgbackrest --stanza=srvpgprd01 --log-level-console=info stop
sudo -u postgres pgbackrest --stanza=srvpgprd01 --repo=1 --log-level-console=info stanza-delete

# Cron para os backups:
0 23 6 * * sudo -iu postgres pgbackrest --compress-level-network=0 --stanza=srvpgprd01 --type=full backup >/dev/null 2>&1
0 23 3 * * sudo -iu postgres pgbackrest --compress-level-network=0 --stanza=srvpgprd01 --type=diff backup >/dev/null 2>&1
