#!/bin/bash

DUMP_DIR="/data/dumps"
LOG_DIR="/data/dumps/logs"
LOG_DUMP="$LOG_DIR/$db.$(date +%d-%m-%Y"_"%H_%M").log"
LOG_DEL="$LOG_DIR/delecao.$db.$(date +%d-%m-%Y"_"%H_%M").log"
PG_PORT=5432


# Verificação de diretórios
if [ ! -d "$DUMP_DIR" ]; then
   echo "Criando diretório /data/dumps: $DUMP_DIR"
   mkdir -p "$DUMP_DIR"
   sudo chown -R postgres:postgres $DUMP_DIR
fi

if [ ! -d "$LOG_DIR" ]; then
   echo "Criando diretório de logs: $LOG_DIR"
   mkdir -p "$LOG_DIR"
   sudo chown -R postgres:postgres $LOG_DIR
fi


# Realização do(s) dump(s).
for db in $(sudo -u postgres psql -p $PG_PORT -t -c "select datname from pg_database where not datistemplate" | grep '\S' | awk '{$1=$1};1'); do
   su - postgres -c "pg_dump -p $PG_PORT -v -d $db -Fd -E UTF-8 -f $DUMP_DIR/$db.$(date +%d-%m-%Y"_"%H_%M)" -j2 > "$LOG_DUMP" 2>&1
done


# Deleção de dump(s) antigo(s).
echo "Deleção de dumps após 30 dias."
find "$DUMP_DIR" -mtime +30 -exec rm {} \; > "$LOG_DEL"
