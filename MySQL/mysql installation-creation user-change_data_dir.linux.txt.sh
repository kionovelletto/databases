###### INSTALANDO O MYSQL SERVER ######

wget https://repo.mysql.com//mysql-apt-config_0.8.26-1_all.deb 
dpkg -i mysql-apt-config_0.8.26-1_all.deb

--Selecione Ubuntu Bionic
--Selecione Mysql 5.7

apt-get update && apt-cache policy mysql-server
sudo apt install -f mysql-client mysql-server

sudo mysql_secure_installation (yes)

systemctl start mysql
systemctl enable mysql

###### CRIANDO USUÁRIO ######

CREATE USER 'username'@'%' IDENTIFIED BY 'user_password';
GRANT ALL ON *.* TO 'username'@'%';
ALTER USER 'username'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
FLUSH PRIVILEGES;

mkdir -p /data/mysql
chown -R mysql:mysql /data/mysql


###### MUDAR O DATADIR DO MYSQL PARA OUTRO LUGAR ######

cp -Rfv /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf.BACKUP


mkdir -p /Novo/Dir/
chown -R mysql:mysql /Novo/Dir/ 


vim /etc/mysql/mysql.conf.d/mysqld.cnf 

[mysqld]
#datadir                = /var/lib/mysql
datadir         = /Novo/Dir/mysqldatabase


sudo vim /etc/apparmor.d/tunables/alias

Or if mysql databases are stored in /home:
alias /var/lib/mysql/ -> /Novo/Dir/,

sudo rsync -av /var/lib/mysql /DADOS/databases/

###### DESATIVANDO SECURE TRANSPORT ######

mysql -u root -p
SET GLOBAL require_secure_transport = 'OFF';

sudo systemctl restart apparmor
sudo systemctl restart mysql



##### MySQL DUMP   #########
vim /etc/mysql/conf.d/mysqldump.cnf

max_allowed_packet = 256M
